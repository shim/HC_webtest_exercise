Feature: Manager Add New Customer

Scenario: Check results on entering valid format for all fields
    Given input Customer Name
    And choose Gender
    And input Address
    And input City
    And input State
    And input PIN
    And input Mobile Number
    And input E-mail
    And input Password
    When Fill all fields in 'Add New Customer' form
    And click button 'submit'
    Then I should see 'Customer Registered Successfully!!!' pop up

Scenario: Check results on entering invalid format
    Given input invalid format for <invalid_field>
    When Fill all fields in 'Add New Customer' form
    Then I should see 'Characters are not allowed' warning

    		Cases:
      | invalid_field |
      | Customer Name |
      | Date of Birth |
      | Address |	
      | City |
      | State |
      | PIN	|
      | Mobile Number |
      | E-mail |
      | Password |

Scenario: Check results on letting one (or more) field(s) empty
    Given entering valid format informations but let one (or more) field(s) empty
    When Fill all fields in 'Add New Customer' form
    And click button 'submit'
    Then I should see 'Please fill all fields!!!' pop up

Scenario: Check results on selecting but let empty and leave in any field(s)
    Given enter the <blank_filed> and leave without input anything
    When Fill all fields in 'Add New Customer' form
    Then I should see '<blank_filed> must not be blank' warning

    		Cases:
      | blank_field |
      | Customer Name |
      | Date of Birth |
      | Address |	
      | City |
      | State |
      | PIN	|
      | Mobile Number |
      | E-mail |
      | Password |