Feature: Manager add amount Deposit of an Account info

Scenario: Check results on entering valid format
    Given input Account No
    And input Amount
    And input Description
    And input
    When Fill all fields in 'Amount Deposit Form' form
    And click button 'submit'
    Then I should see 'Transaction details of Deposit for Account [No of acc]' pop up

Scenario: Check results on entering invalid format
    Given input invalid format for <invalid_field>
    When Fill any field in 'Amount Deposit Form' form
    Then I should see 'Characters are not allowed' warning

    		Cases:
      | invalid_field |
      | Account No |
      | Amount |
      | Description |

Scenario: Check results on letting one (or more) field(s) empty
    Given entering valid format informations but let one (or more) field(s) empty
    When Fill all fields in 'Amount Deposit Form' form
    And click button 'submit'
    Then I should see 'Please fill all fields!!!' pop up

Scenario: Check results on selecting but let empty and leave in any field(s)
    Given enter the <blank_filed> and leave without input anything
    When Fill all fields in 'Amount Deposit Form' form
    Then I should see '<blank_filed> must not be blank' warning

    		Cases:
      | blank_field |
      | Account No |
      | Amount |
      | Description |