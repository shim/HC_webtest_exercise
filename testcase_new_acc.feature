Feature: Manager Add New Account

Scenario: Check results on entering valid format
    Given input Customer id
    And choose Account type
    And input Initial deposit
    When Fill all fields in 'Add new account form' form
    And click button 'submit'
    Then I should see 'Account Generated Successfully!!!' pop up

Scenario: Check results on entering invalid format
    Given input invalid format for <invalid_field>
    When Fill all fields in 'Add new account form' form
    Then I should see 'Characters are not allowed' warning

    		Cases:
      | invalid_field |
      | Customer id |
      | Initial deposit | 	

Scenario: Check results on letting one (or more) field(s) empty
    Given entering valid format informations but let one (or more) field(s) empty
    When Fill all fields in 'Add new account form' form
    And click button 'submit'
    Then I should see 'Please fill all fields!!!' pop up

Scenario: Check results on selecting but let empty and leave in any field(s)
    Given enter the <blank_filed> and leave without input anything
    When Fill all fields in 'Add new account form' form
    Then I should see '<blank_filed> must not be blank' warning

    		Cases:
      | blank_field |
      | Customer id |
      | Initial deposit |