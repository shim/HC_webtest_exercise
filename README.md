#### Webtest

1) Goal: The manager (after login) can be successfuly in:

	1.1 Add new customer
	1.2 Add new account
	1.3 Add deposit info

2) Features:

	21 Manager Add New Customer

		2.1.1. Check results on entering valid format for all fields
		2.1.2. Check results on entering invalid format
		2.1.3. Check results on letting one (or more) field(s) empty
		2.1.4. Check results on selecting but let empty and leave in any field(s)

	2.2 Manager Add New Account

		2.2.1. Check results on entering valid format for all fields
		2.2.2. Check results on entering invalid format
		2.2.3. Check results on letting one (or more) field(s) empty
		2.2.4. Check results on selecting but let empty and leave in any field(s)

	2.3 Manager add amount Deposit of an Account info

		2.3.1. Check results on entering valid format for all fields
		2.3.2. Check results on entering invalid format
		2.3.3. Check results on letting one (or more) field(s) empty
		2.3.4. Check results on selecting but let empty and leave in any field(s)