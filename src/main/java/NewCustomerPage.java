import org.openqa.selenium.WebDriver;

/**
 * Created by shim on 8/6/17.
 */
public class NewCustomerPage {
    private WebDriver wd;
    private String customerId;

    public NewCustomerPage(WebDriver wd, String customerId) throws IllegalStateException{
        this.wd = wd;
        this.customerId = customerId;
        if (!wd.getTitle().equals("Guru99 Bank New Customer Entry Page")){
            throw new IllegalStateException("This is not New Customer Entry Page");
        }
    }

    public NewCustomerPage createNewCustomer(/*TODO: add customer info*/){

        return new NewCustomerPage(wd);
    }
}
