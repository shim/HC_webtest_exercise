import org.openqa.selenium.WebDriver;

/**
 * Created by shim on 8/6/17.
 */
public class DepositPage {
    private WebDriver wd;
    private String accountId;

    public NewCustomerPage(WebDriver wd, String accountId) throws IllegalStateException{
        this.wd = wd;
        this.accountId = accountId;
        if (!wd.getTitle().equals("Guru99 Bank Deposit Entry Page")){
            throw new IllegalStateException("This is not Deposit Entry Page");
        }
    }

    public DepositPage addNewDeposit(/*TODO: add deposit info*/){

        return new DepositPage(wd);
    }
}
