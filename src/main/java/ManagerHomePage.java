import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by shim on 8/6/17.
 */
public class ManagerHomePage {
    private WebDriver wd;

    public ManagerHomePage(WebDriver wd) throws IllegalStateException{
        this.wd = wd;
        if (!wd.getTitle().equals("Guru99 Bank Manager HomePage")) {
            throw new IllegalStateException("This is not the Manager HomePage!");
        }
    }

    public void runSite(){
        System.out.println("We're in the Manager HomePage")
    }

    public NewCustomerPage createNewCustomer(String customerName, boolean gender, Date dob, String address, String city, String state, String pin, String mobile, String email, String password){
        WebElement we, checkWe; //TODO: use checkWe to check visible error labels
        we = wd.findElement(By.name("name"));
        we.sendKeys(customerName);
        ArrayList<WebElement> weList;
        weList = (ArrayList<WebElement>) wd.findElements(By.name("rad1"));
        if (gender){
            weList.get(0).click();
        } else {
            weList.get(1).click();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
        we = wd.findElement(By.id("dob"));
        we.sendKeys(sdf.format(dob));
        we = wd.findElement(By.name("addr"));
        we.sendKeys(address);
        we = wd.findElement(By.name("city"));
        we.sendKeys(city);
        we = wd.findElement(By.name("state"));
        we.sendKeys(state);
        we = wd.findElement(By.name("pinno"));
        we.sendKeys(pin);
        we = wd.findElement(By.name("telephoneno"));
        we.sendKeys(mobile);
        we = wd.findElement(By.name("emailid"));
        we.sendKeys(email);
        we = wd.findElement(By.name("password"));
        we.sendKeys(password);
        we = wd.findElement(By.name("sub"));
        we.click();
        if (!wd.getTitle().equals("")){
            throw new IllegalStateException("New Customer failed");
        }
        return new NewCustomerPage(wd);
    }
}
