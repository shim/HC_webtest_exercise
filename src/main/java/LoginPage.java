import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by shim on 8/6/17.
 */
public class LoginPage {
    private WebDriver wd;

    public LoginPage(WebDriver wd) throws IllegalStateException{
        this.wd = wd;
        try{
            Assert.assertTrue(wd.getCurrentUrl().equals("http://demo.guru99.com/v4/"));
        }
        catch (AssertionError ae){
            throw new IllegalStateException("This is not the login page!");
        }
    }

    public ManagerHomePage loginAttempt(String username, String password){
        WebElement we = wd.findElement(By.name("uid"));
        we.sendKeys(username);
        we = wd.findElement(By.name("password"));
        we.sendKeys(password);
        we.submit();
        return new ManagerHomePage(wd);
    }
}
