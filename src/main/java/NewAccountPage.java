import org.openqa.selenium.WebDriver;

/**
 * Created by shim on 8/6/17.
 */
public class NewAccountPage {
    WebDriver wd;
    public NewAccountPage(WebDriver wd) throws IllegalStateException{
        this.wd = wd;
        if (!wd.getTitle().equals("Guru99 bank add new account")){
            throw new IllegalStateException("This is not the New Account Page");
        }
    }

public NewAccountPage createNewAcount(/*TODO: add account info*/){

        return new NewAccountPage(wd);
    }
}
